*** Keywords ***

LAUNCH APP
    set serial  ${TEST_DEVICE}
    execute adb shell command  pm grant com.agence3pp android.permission.ACCESS_FINE_LOCATION
    execute adb shell command  pm grant com.agence3pp android.permission.READ_PHONE_STATE
    execute adb shell command  monkey -p com.agence3pp -v 1   

CLOSE APP
    set serial  ${TEST_DEVICE}
    execute adb shell command  am force-stop com.agence3pp