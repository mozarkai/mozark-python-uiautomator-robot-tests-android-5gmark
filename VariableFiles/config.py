import os

TEST_DEVICE = os.environ['DEVICE_SERIAL_ID']
appPackage = 'com.agence3pp'
appActivity = 'qosi.fr.usingqosiframework.splashscreen.SplashscreenActivity'

ALERT = {
	"resourceId": 'com.agence3pp:id/alertTitle'
}

ALERT_OK = {
	"resourceId": 'android:id/button1'
}

TEST_TAB = {
	"resourceId": 'com.agence3pp:id/navigation_3'
}

FULL_TEST = {
	"text": 'Full test'
}

SPEED_TEST = {
	"text": 'Speed test'
}

TEST_BUTTON = {
	"text": 'TEST'
}

INDOOR_BUTTON = {
	"text": 'INDOOR'
}

OUTDOOR_BUTTON = {
	"text": 'OUTDOOR'
}

RESULT_BUTTON = {
	"text": 'RESULTS'
}

EXPLANATIONS_BUTTON = {
	"text": 'EXPLANATIONS'
}

DOWNLOAD_RESULT = '//android.view.ViewGroup[1]/android.widget.TextView[2]'
UPLOAD_RESULT = '//android.view.ViewGroup[2]/android.widget.TextView[2]'
STREAM_RESULT = '//android.view.ViewGroup[3]/android.widget.TextView[2]'
WEB_RESULT = '//android.view.ViewGroup[4]/android.widget.TextView[2]'

HISTORY_TAB = {
	"resourceId": 'com.agence3pp:id/navigation_2'
}

ACCOUNT_SETTINGS_TAB = {
	"resourceId": 'com.agence3pp:id/navigation_5'
}

DISPLAYED_DATA_UNIT = {
	"text": 'Displayed data unit'
}

last_UNIT = {
	"text": 'mBps'
}

Mbps_UNIT = {
	"text": 'Mbps'
}

FIRST_RESULT = {
	"resourceId": 'com.agence3pp:id/id_item_history_detail_date_tv'
}