*** Settings ***
Library         uiautomatorlibrary
Library         String
Library         OperatingSystem
Library         DateTime
Library         Collections
Resource        ${EXECDIR}/Resources/resource.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC 001: Full Indoor Test
  [Tags]  FullIndoorTest
    LAUNCH APP
    click  &{TEST_TAB}
    click  &{FULL_TEST}
    click  &{TEST_BUTTON}
    wait for exists  5000  &{INDOOR_BUTTON}
    click  &{INDOOR_BUTTON}
    wait for exists  240000  &{RESULT_BUTTON}
    click  &{EXPLANATIONS_BUTTON}
    builtin.sleep  5
    press back
    wait for exists  3000  &{HISTORY_TAB}
    click  &{HISTORY_TAB}
    wait for exists  5000  &{FIRST_RESULT}
    click  &{FIRST_RESULT}
    run keyword if  ${RESULT_BUTTON}  log to console  5Gmark Test Passed
    CLOSE APP