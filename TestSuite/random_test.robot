*** Settings ***
Library         uiautomatorlibrary
Library         String
Library         OperatingSystem
Library         DateTime
Library         Collections
Resource        ${EXECDIR}/Resources/resource.robot
Variables       ${EXECDIR}/VariableFiles/config.py

*** Test Cases ***

TC_001: Change Mbps To mBps And Verify The Same In History Tab
  [Tags]  MbpsTomBps
  LAUNCH APP
  wait for exists  10000  &{ACCOUNT_SETTINGS_TAB}
  click  &{ACCOUNT_SETTINGS_TAB}
  wait for exists  5000  &{DISPLAYED_DATA_UNIT}
  click  &{DISPLAYED_DATA_UNIT}
  wait for exists  5000  &{last_UNIT}
  click  &{last_UNIT}
  wait for exists  5000  &{TEST_TAB}
  click  &{TEST_TAB}
  click  &{SPEED_TEST}
  click  &{TEST_BUTTON}
  wait for exists  5000  &{INDOOR_BUTTON}
  click  &{INDOOR_BUTTON}
  wait for exists  180000  &{RESULT_BUTTON}
  click  &{EXPLANATIONS_BUTTON}
  builtin.sleep  5
  press back
  wait for exists  5000  &{ACCOUNT_SETTINGS_TAB}
  click  &{ACCOUNT_SETTINGS_TAB}
  wait for exists  5000  &{DISPLAYED_DATA_UNIT}
  click  &{DISPLAYED_DATA_UNIT}
  wait for exists  5000  &{Mbps_UNIT}
  click  &{Mbps_UNIT}
  CLOSE APP

TC_002: Speed Outdoor Test
  [Tags]  SpeedOutdoorTest
  LAUNCH APP
  wait for exists  5000  &{TEST_TAB}
  click  &{TEST_TAB}
  click  &{SPEED_TEST}
  click  &{TEST_BUTTON}
  wait for exists  5000  &{OUTDOOR_BUTTON}
  click  &{OUTDOOR_BUTTON}
  wait for exists  180000  &{RESULT_BUTTON}
  click  &{EXPLANATIONS_BUTTON}
  builtin.sleep  5
  CLOSE APP